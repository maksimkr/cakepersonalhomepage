<?php
class MessagesController extends AppController{
    public $helpers = array('Html', 'Form');

    public function index()
    {
        $this->set('messages', $this->Message->find('all'));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Message->create();
            if ($this->Message->save($this->request->data)) {
                $this->Flash->success(__('Your message has been send.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to send your message.'));
        }
    }

    public function admin_delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Message->delete($id)) {
            $this->Flash->success(
                __('The message with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Flash->error(
                __('The message with id: %s could not be deleted.', h($id))
            );
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_index() {
        $this->set('messages', $this->Message->find('all'));
    }
}