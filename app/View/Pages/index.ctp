<!DOCTYPE html>
<html lang="en">
    <head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Home</title>
		<link rel="shortcut icon" href="x-7.ico" type="image/x-icon" />
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Home<span class="sr-only">(current)</span></a></li>
						<li><a href="/work">Portfolio</a></li>
						<li><a href="/messages">Contacts</a></li>
						<li><a href="/users/login">Admin panel</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<div class="container-fluid">
			<div class="row row-offcanvas row-offcanvas-left">
				<div class="col-sm-3 col-md-2 sidebar-offcanvas">
					<img src="//placehold.it/200/6666ff/fff" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
				</div><!--/span-->
				<div class="col-sm-9 col-md-10 main">
					<div class="jumbotron">
						<div class="container">
							<div class="panel panel-default">
								<div class="panel-heading">Info about owner</div>
  									<div class="panel-body">
											Panel content
  									</div>
								</div>
							<div class="panel panel-default">
								<div class="panel-heading">Information about the work experience and education</div>
  								<div class="panel-body">
											Panel content
  								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">Panel heading without title</div>
  								<div class="panel-body">
										Panel content
  								</div>
							</div>
  						</div>
					</div>
				</div>
			</div>
		</div><!--/.container-->
		<footer>
			<p class="pull-left">MegaSuperCompany 2015</p>
		</footer>
	</body>
</html>