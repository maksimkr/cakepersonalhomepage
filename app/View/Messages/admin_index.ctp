<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Contacts</title>
		<link rel="shortcut icon" href="x-7.ico" type="image/x-icon" />
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="/">Home</a></li>
						<li><a href="/work">Portfolio</a></li>
						<li><a href="/messages">Contacts</a></li>
						<li class="active"><a href="/users/login">Admin panel<span class="sr-only">(current)</span></a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
        <div class="row">
            <div class="col-md-12">
                <div class="well  well-sm">
                    <div>
                        <legend class="text-center header">Admin Panel/<?php
                                                                        echo $this->Html->link('Log out', array('controller' => 'users', 'action' => 'logout'));
                                                                        ?>
                        </legend>
                        <table>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Message</th>
                                <th>Delete</th>
                            </tr>
                            <?php foreach ($messages as $message): ?>
                            <tr>
                                <td><?php echo $message['Message']['id']; ?></td>
                                <td><?php echo $message['Message']['name']; ?></td>
                                <td><?php echo $message['Message']['email']; ?></td>
                                <td><?php echo $message['Message']['message']; ?></td>
                                <td><?php echo $this->Form->postLink(
                                                    'Delete',
                                                    array('action' => 'delete', $message['Message']['id']),
                                                    array('confirm' => 'Are you sure?')
                                                );
                                    ?></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php unset($message); ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <p class="pull-left">MegaSuperCompany 2015</p>
        </footer>
    </body>
</html>