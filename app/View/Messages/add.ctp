<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Contacts</title>
		<link rel="shortcut icon" href="x-7.ico" type="image/x-icon" />
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="/">Home</a></li>
						<li><a href="/work">Portfolio</a></li>
						<li class="active"><a href="/messages">Contacts<span class="sr-only">(current)</span></a></li>
						<li><a href="/users/login">Admin panel</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<div class="well">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <legend class="text-center header">Contacts are:</legend>
                            <p>Vk.com: <a href="#">vk.com/id0</a></p>
                            <p>Skype:    	<a href="skype:maksim.kremlev?chat">maksim.kremlev</a></p>
                            <p>Gmail: 	<a href="mailto:maksimkremljov53@gmail.com">maksimkremljov53@gmail.com</a></p>
                    </div>
                    <div class="col-md-8">
                        <legend class="text-center header">Contact us</legend>
                        <?php
                        echo $this->Form->create('Message');
                        echo $this->Form->input('name');
                        echo $this->Form->input('email');
                        echo $this->Form->input('message');
                        echo $this->Form->end('Sent Message');
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <p class="pull-left">MegaSuperCompany 2015</p>
        </footer>
    </body>
</html>