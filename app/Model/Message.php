<?php
class Message extends AppModel {
    public $validate = array(
        'message' => array(
            'rule' => 'notBlank',
            'rule' => array('maxLength', 500),
            'message' => 'Message must be no larger than 500 characters long.'
        ),
        'email' => array(
            'rule' => 'notBlank'
        )
    );
}
